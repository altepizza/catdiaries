# Setup

pip3 install -r REQUIREMENTS.txt
docker-compose -f docker-compose.dev.yml up -d
MONGO_DB_HOST=localhost uvicorn main:app --reload