import os
from datetime import datetime
from bson import ObjectId
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field
from pymongo import MongoClient
from typing import Optional

MONGODB_HOST = os.environ['MONGO_DB_HOST']
client = MongoClient(f'mongodb://root:example@{MONGODB_HOST}', 27017)
db = client.test
app = FastAPI()

# TODO
origins = [
    # 'http://localhost:3000',
    # 'http://127.0.0.1:1337',
    # 'http://localhost:1337',
    '*'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class PyObjectId(ObjectId):

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError('Invalid objectid')
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')


class Cat(BaseModel):
    id: Optional[PyObjectId] = Field(alias='_id')
    name: str

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            ObjectId: str
        }


class Feeding(BaseModel):
    id: Optional[PyObjectId] = Field(alias='_id')
    cat_name: str
    date_time: Optional[datetime] = None

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            ObjectId: str
        }


class Throw_Up(BaseModel):
    id: Optional[PyObjectId] = Field(alias='_id')
    cat_name: str
    date_time: Optional[datetime] = None

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            ObjectId: str
        }


# /cats

@app.get("/cats")
async def list_cats():
    cats = []
    for cat in db.cats.find():
        cats.append(Cat(**cat))
    return {'cats': cats}


@app.post("/cats")
async def create_cat(cat: Cat):
    if hasattr(cat, 'id'):
        delattr(cat, 'id')
    ret = db.cats.insert_one(cat.dict(by_alias=True))
    cat.id = ret.inserted_id
    return {'cat': cat}


@app.delete("/cats/{cat_name}")
async def delete_cat(cat_name):
    query = {"name": cat_name}
    db.cats.delete_one(query)
    return


# /feedings

@app.get("/feedings")
async def list_feedings():
    feedings = []
    for feeding in db.feedings.find():
        feedings.append(Feeding(**feeding))
    return {'feedings': feedings}


@app.post("/feedings")
async def create_feeding(feeding: Feeding):
    if hasattr(feeding, 'id'):
        delattr(feeding, 'id')
    if not feeding.date_time:
        feeding.date_time = datetime.now()
    ret = db.feedings.insert_one(feeding.dict(by_alias=True))
    feeding.id = ret.inserted_id
    return {'feeding': feeding}


# /throw_ups

@app.get("/throw_ups")
async def list_throw_ups():
    throw_ups = []
    for throw_up in db.throw_ups.find():
        throw_ups.append(Throw_Up(**throw_up))
    return {'throw_ups': throw_ups}


@app.post("/throw_ups")
async def create_throw_up(throw_up: Throw_Up):
    if hasattr(throw_up, 'id'):
        delattr(throw_up, 'id')
    if not throw_up.date_time:
        throw_up.date_time = datetime.now()
    ret = db.throw_ups.insert_one(throw_up.dict(by_alias=True))
    throw_up.id = ret.inserted_id
    return {'throw_up': throw_up}
