import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';


class CatFeeder extends React.Component {

  ApiBaseURL = process.env.REACT_APP_API_URL

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      cats: [],
      feedings: [],
      throwUps: []
    };
  }

  componentDidMount() {
    fetch(this.ApiBaseURL + "/cats")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            cats: result.cats
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )

    fetch(this.ApiBaseURL + "/feedings")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            feedings: result.feedings
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )

    fetch(this.ApiBaseURL + "/throw_ups")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            throwUps: result.throw_ups
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  feedACat(cat) {
    console.log(cat.name)
    fetch(this.ApiBaseURL + '/feedings', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cat_name: cat.name,
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          const newFeedings = [...this.state.feedings, result.feeding];
          this.setState({
            feedings: newFeedings
          })
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  catThrowsUp(cat) {
    console.log(cat.name)
    fetch(this.ApiBaseURL + '/throw_ups', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cat_name: cat.name,
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          const newThrowUps = [...this.state.throwUps, result.throw_up];
          this.setState({
            throwUps: newThrowUps
          })
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  deleteACat(cat) {
    console.log(cat.name)
    fetch(this.ApiBaseURL + '/cats/' + cat.name, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then(res => res.json())
      .then(
        (result) => {
          const newCats = this.state.cats.filter(tmp_cat => tmp_cat.name !== cat.name);
          this.setState({
            cats: newCats
          })
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  getLatestFeedingDateFor(cat) {
    const feedingsForCat = this.state.feedings.filter(feeding => feeding.cat_name === cat.name);
    if (feedingsForCat.length) {
      const latestDate = new Date(Math.max(...feedingsForCat.map(e => new Date(e.date_time))));
      console.log(latestDate);
      return latestDate.toString()
    }
    return "Never"
  }

  getLatestThrowUpDateFor(cat) {
    const throwUpsForCat = this.state.throwUps.filter(throwUp => throwUp.cat_name === cat.name);
    if (throwUpsForCat.length) {
      const latestDate = new Date(Math.max(...throwUpsForCat.map(e => new Date(e.date_time))));
      console.log(latestDate);
      return latestDate.toString()
    }
    return "Never"
  }

  render() {
    const { error, isLoaded, cats } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <ul>
          {cats.map(cat => (<>
            <li key={cat.id}>
              <h1>{cat.name}</h1>
              <b>Last feeding:</b> {this.getLatestFeedingDateFor(cat)}<br />
              <b>Last throw up:</b> {this.getLatestThrowUpDateFor(cat)}<br />
            </li>
            <p>
              <button onClick={() => { this.feedACat(cat) }}>Feed Me!</button>
            </p>
            <p>
              <button onClick={() => { this.catThrowsUp(cat) }}>Vomit</button>
            </p>
            <p>
              <button onClick={() => { this.deleteACat(cat) }}>Delete This Cat</button>
            </p>
          </>
          ))
          }
        </ul >
      );
    }
  }
}

ReactDOM.render(
  <React.StrictMode>
    <CatFeeder />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
